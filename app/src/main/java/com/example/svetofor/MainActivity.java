package com.example.svetofor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private ConstraintLayout cl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cl = findViewById(R.id.ConstraintLayout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void onClickButton(View view) {

        switch (view.getId()){
            case R.id.buttonRed:
                cl.setBackgroundColor(ContextCompat.getColor(this,R.color.colorRed));
                break;
            case R.id.buttonYellow:
                cl.setBackgroundColor(ContextCompat.getColor(this,R.color.colorYellow));
                break;
            case R.id.buttonGreen:
                cl.setBackgroundColor(ContextCompat.getColor(this,R.color.colorGreen));
                break;
        }
    }
}
